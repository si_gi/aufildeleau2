package com.example.si_gi.aufildeleau;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;



public class CommunesFragment extends Fragment implements AdapterView.OnItemClickListener {
    ListView lv;
    ArrayList<String> communes = new ArrayList<>();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CommunesFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CommunesFragment newInstance(String param1, String param2) {
        CommunesFragment fragment = new CommunesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private ArrayList<String> listeCommunes(Context context){
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open("points_interets_communes.json")));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        }catch(IOException e){
            e.printStackTrace();
        } finally {
            try {
                br.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        String myjsonstring = sb.toString();

        try{
            JSONObject jsonRootObject = new JSONObject(myjsonstring);
            Iterator<?> keys = jsonRootObject.keys();

            while( keys.hasNext() ) {
                String key = (String) keys.next();
                communes.add(key);
               // System.out.println("Key: " + key);
               // System.out.println("Value: " + jsonRootObject.get(key));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return communes;
    }







    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_communes, container, false);
        listeCommunes(getContext());
        ArrayAdapter adtr = new ArrayAdapter<String>(getActivity(), R.layout.liste_communes, communes);
        lv = v.findViewById(R.id.list);
        lv.setAdapter(adtr);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                PointsInteretFragment frag = new PointsInteretFragment();
                Bundle bundle = new Bundle();
                bundle.putString("ville", communes.get(i));
                frag.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, frag).commit();
            }
        });
        return v;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent pointinteret = new Intent(getContext(),PointInteretActivity.class);
        pointinteret.putExtra("commune",communes.get(i));
        startActivityForResult(pointinteret,100);

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
