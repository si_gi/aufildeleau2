package com.example.si_gi.aufildeleau;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;



public class PointsInteretFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PointsInteretFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PointsInteretFragment newInstance(String param1, String param2) {
        PointsInteretFragment fragment = new PointsInteretFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("ville_test_arg", getArguments().getString("ville"));

        getPointsInteret();
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_points_interet, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public ArrayList<PointInteret> getPointsInteret() {
        StringBuffer sb = new StringBuffer();
        ArrayList<PointInteret> arrayList = new ArrayList<PointInteret>();
        try {
            InputStreamReader reader = new InputStreamReader(getContext().getAssets().open("JSON_Allege.json"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String ligne = "";
            ;
            while ((ligne = bufferedReader.readLine()) != null) {
                sb.append(ligne);
            }
            bufferedReader.close();
        } catch (Exception e) {
            Log.i("ERROR", e.getMessage());
        }
        String myjsonstring = sb.toString();

//        TODO : FIX JSON READ BUG

        try{
            JSONObject jsonRootObject = new JSONObject(myjsonstring);
            Log.i("eeeeeee",jsonRootObject.toString());
            Iterator<?> keys = jsonRootObject.keys();

            while( keys.hasNext() ) {
                String key = (String) keys.next();
                Log.i("KEYKEYKEY", key);
                // System.out.println("Key: " + key);
                // System.out.println("Value: " + jsonRootObject.get(key));
            }

        } catch (JSONException e) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter("stacktrace.txt", "UTF-8");
                writer.println(e.getMessage());
                writer.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }


        }

        return arrayList;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
